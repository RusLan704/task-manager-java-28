package ru.bakhtiyarov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-02-06T01:23:57.890+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "UserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface UserEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/updateUserPasswordRequest", output = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/updateUserPasswordResponse")
    @RequestWrapper(localName = "updateUserPassword", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.UpdateUserPassword")
    @ResponseWrapper(localName = "updateUserPasswordResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.UpdateUserPasswordResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.UserDTO updateUserPassword(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/updateUserLastNameRequest", output = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/updateUserLastNameResponse")
    @RequestWrapper(localName = "updateUserLastName", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.UpdateUserLastName")
    @ResponseWrapper(localName = "updateUserLastNameResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.UpdateUserLastNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.UserDTO updateUserLastName(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "lastName", targetNamespace = "")
        java.lang.String lastName
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/updateUserLoginRequest", output = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/updateUserLoginResponse")
    @RequestWrapper(localName = "updateUserLogin", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.UpdateUserLogin")
    @ResponseWrapper(localName = "updateUserLoginResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.UpdateUserLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.UserDTO updateUserLogin(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/updateUserEmailRequest", output = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/updateUserEmailResponse")
    @RequestWrapper(localName = "updateUserEmail", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.UpdateUserEmail")
    @ResponseWrapper(localName = "updateUserEmailResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.UpdateUserEmailResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.UserDTO updateUserEmail(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "email", targetNamespace = "")
        java.lang.String email
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/createUserByLoginPasswordRequest", output = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/createUserByLoginPasswordResponse")
    @RequestWrapper(localName = "createUserByLoginPassword", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.CreateUserByLoginPassword")
    @ResponseWrapper(localName = "createUserByLoginPasswordResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.CreateUserByLoginPasswordResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.UserDTO createUserByLoginPassword(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/updateUserFirstNameRequest", output = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/updateUserFirstNameResponse")
    @RequestWrapper(localName = "updateUserFirstName", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.UpdateUserFirstName")
    @ResponseWrapper(localName = "updateUserFirstNameResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.UpdateUserFirstNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.UserDTO updateUserFirstName(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "firstName", targetNamespace = "")
        java.lang.String firstName
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/createUserByLoginPasswordEmailRequest", output = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/createUserByLoginPasswordEmailResponse")
    @RequestWrapper(localName = "createUserByLoginPasswordEmail", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.CreateUserByLoginPasswordEmail")
    @ResponseWrapper(localName = "createUserByLoginPasswordEmailResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.CreateUserByLoginPasswordEmailResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.UserDTO createUserByLoginPasswordEmail(
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password,
        @WebParam(name = "email", targetNamespace = "")
        java.lang.String email
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/findUserByLoginRequest", output = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/findUserByLoginResponse")
    @RequestWrapper(localName = "findUserByLogin", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.FindUserByLogin")
    @ResponseWrapper(localName = "findUserByLoginResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.FindUserByLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.UserDTO findUserByLogin(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/createUserByLoginPasswordRoleRequest", output = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/createUserByLoginPasswordRoleResponse")
    @RequestWrapper(localName = "createUserByLoginPasswordRole", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.CreateUserByLoginPasswordRole")
    @ResponseWrapper(localName = "createUserByLoginPasswordRoleResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.CreateUserByLoginPasswordRoleResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.UserDTO createUserByLoginPasswordRole(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password,
        @WebParam(name = "email", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.Role email
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/findUserByIdRequest", output = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/findUserByIdResponse")
    @RequestWrapper(localName = "findUserById", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.FindUserById")
    @ResponseWrapper(localName = "findUserByIdResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.FindUserByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.UserDTO findUserById(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/updateUserMiddleNameRequest", output = "http://endpoint.tm.bakhtiyarov.ru/UserEndpoint/updateUserMiddleNameResponse")
    @RequestWrapper(localName = "updateUserMiddleName", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.UpdateUserMiddleName")
    @ResponseWrapper(localName = "updateUserMiddleNameResponse", targetNamespace = "http://endpoint.tm.bakhtiyarov.ru/", className = "ru.bakhtiyarov.tm.endpoint.UpdateUserMiddleNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bakhtiyarov.tm.endpoint.UserDTO updateUserMiddleName(
        @WebParam(name = "session", targetNamespace = "")
        ru.bakhtiyarov.tm.endpoint.SessionDTO session,
        @WebParam(name = "middleName", targetNamespace = "")
        java.lang.String middleName
    );
}
