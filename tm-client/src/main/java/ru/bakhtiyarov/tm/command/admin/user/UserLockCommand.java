package ru.bakhtiyarov.tm.command.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.AdminUserEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.UserDTO;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "lock-user";
    }

    @NotNull
    @Override
    public String description() {
        return "Lock user in program.";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("[ENTER LOGIN]");
        @NotNull final String login = TerminalUtil.nextLine();
        @Nullable final AdminUserEndpoint adminUserEndpoint = endpointLocator.getAdminUserEndpoint();
        @NotNull SessionDTO session = serviceLocator.getSessionService().getSession();
        UserDTO user = adminUserEndpoint.lockUserByLogin(session, login);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}