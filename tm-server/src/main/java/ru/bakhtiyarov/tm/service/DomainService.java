package ru.bakhtiyarov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.service.IDomainService;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.constant.DataConst;
import ru.bakhtiyarov.tm.dto.DomainDTO;
import ru.bakhtiyarov.tm.exception.data.DataLoadException;
import ru.bakhtiyarov.tm.exception.data.DataSaveException;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.imageio.IIOException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class DomainService implements IDomainService {

    @NotNull
    private final ServiceLocator serviceLocator;

    @NotNull
    public DomainService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void loadBase64() throws Exception {
        @NotNull final String base64date = new String(Files.readAllBytes(Paths.get(DataConst.FILE_BASE64)));
        @NotNull final byte[] decodeData = new BASE64Decoder().decodeBuffer(base64date);
        try (
                @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodeData);
                @NotNull final ObjectInputStream objectOutputStream = new ObjectInputStream(byteArrayInputStream)
        ) {
            @NotNull final DomainDTO domain = (DomainDTO) objectOutputStream.readObject();
            setDomain(domain);
        } catch (IIOException e) {
            throw new DataLoadException(e);
        }
    }

    @Override
    public void clearBase64() throws Exception {
        @NotNull final File file = new File(DataConst.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public void saveBase64() throws Exception {
        @NotNull final DomainDTO domain = getDomain();
        @NotNull final File file = new File(DataConst.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        try (
                @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file)
        ) {
            objectOutputStream.writeObject(domain);
            @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
            @NotNull final String base64 = new BASE64Encoder().encode(bytes);
            fileOutputStream.write(base64.getBytes());
        } catch (IIOException e) {
            throw new DataSaveException(e);
        }
    }

    @NotNull
    private DomainDTO getDomain() {
        @NotNull final DomainDTO domain = new DomainDTO();
        domain.setProject(serviceLocator.getProjectService().findAll());
        domain.setTask(serviceLocator.getTaskService().findAll());
        domain.setUser(serviceLocator.getUserService().findAll());
        domain.setSessions(serviceLocator.getSessionService().findAll());
        return domain;
    }

    private void setDomain(@Nullable final DomainDTO domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().removeAll();
        serviceLocator.getProjectService().addAll(domain.getProject());
        serviceLocator.getTaskService().removeAll();
        serviceLocator.getTaskService().addAll(domain.getTask());
        serviceLocator.getUserService().removeAll();
        serviceLocator.getUserService().addAll(domain.getUser());
        serviceLocator.getSessionService().removeAll();
        serviceLocator.getSessionService().addAll(domain.getSessions());
    }

    @Override
    public void clearBinary() throws Exception {
        @NotNull final File file = new File(DataConst.FILE_BINARY);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public void loadBinary() throws Exception {
        try (
                @NotNull final FileInputStream fileInputStream = new FileInputStream(DataConst.FILE_BINARY);
                @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)
        ) {
            @NotNull final DomainDTO domain = (DomainDTO) objectInputStream.readObject();
            setDomain(domain);
        } catch (IIOException e) {
            throw new DataLoadException(e);
        }
    }

    @Override
    public void saveBinary() throws Exception {
        @NotNull final DomainDTO domain = getDomain();
        @NotNull final File file = new File(DataConst.FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        try (
                @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
                @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)
        ) {
            objectOutputStream.writeObject(domain);
            fileOutputStream.flush();

        } catch (Exception e) {
            throw new DataSaveException(e);
        }
    }

    @Override
    public void clearJson() throws Exception {
        @NotNull final File file = new File(DataConst.FILE_JSON);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public void loadJson() throws Exception {
        try (
                @NotNull final FileInputStream fileInputStream = new FileInputStream(DataConst.FILE_JSON)
        ) {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final DomainDTO domain = objectMapper.readValue(fileInputStream, DomainDTO.class);
            setDomain(domain);
        } catch (IIOException e) {
            throw new DataLoadException(e);
        }
    }

    @Override
    public void saveJson() throws Exception {
        @NotNull final DomainDTO domain = getDomain();

        @NotNull final File file = new File(DataConst.FILE_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        try (
                @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(DataConst.FILE_JSON);
        ) {
            fileOutputStream.write(json.getBytes());
            fileOutputStream.flush();
        } catch (IIOException e) {
            throw new DataSaveException(e);
        }
    }

    @Override
    public void clearXml() throws Exception {
        final File file = new File(DataConst.FILE_XML);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public void loadXml() throws Exception {
        try (
                @NotNull final FileInputStream fileInputStream = new FileInputStream(DataConst.FILE_XML)
        ) {
            @NotNull final ObjectMapper objectMapper = new XmlMapper();
            @NotNull final DomainDTO domain = objectMapper.readValue(fileInputStream, DomainDTO.class);
            setDomain(domain);
        } catch (IIOException e) {
            throw new DataLoadException(e);
        }
    }

    @Override
    public void saveXml() throws Exception {
        @NotNull final DomainDTO domain = getDomain();

        @NotNull final File file = new File(DataConst.FILE_XML);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        try (
                @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(DataConst.FILE_XML);
        ) {
            fileOutputStream.write(xml.getBytes());
            fileOutputStream.flush();
        } catch (IIOException e) {
            throw new DataSaveException(e);
        }
    }

    @Override
    public void clearYaml() throws Exception {
        @NotNull final File file = new File(DataConst.FILE_YAML);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public void loadYaml() throws Exception {
        try (
                @NotNull final FileInputStream fileInputStream = new FileInputStream(DataConst.FILE_YAML)
        ) {
            YAMLFactory yamlFactory = new YAMLFactory();
            @NotNull final ObjectMapper objectMapper = new ObjectMapper(yamlFactory);
            @NotNull final DomainDTO domain = objectMapper.readValue(fileInputStream, DomainDTO.class);
            setDomain(domain);
        } catch (IIOException e) {
            throw new DataLoadException(e);
        }
    }

    @Override
    public void saveYaml() throws Exception {
        @NotNull final DomainDTO domain = getDomain();
        @NotNull final File file = new File(DataConst.FILE_YAML);

        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        YAMLFactory yamlFactory = new YAMLFactory();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper(yamlFactory);
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        try (
                @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(DataConst.FILE_YAML);
        ) {
            fileOutputStream.write(json.getBytes());
            fileOutputStream.flush();
        } catch (IIOException e) {
            throw new DataSaveException(e);
        }
    }

}
