package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.repository.IRepository;
import ru.bakhtiyarov.tm.dto.AbstractEntityDTO;
import ru.bakhtiyarov.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public  class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    private final EntityManager em;

    public AbstractRepository(@NotNull EntityManager em) {
        this.em = em;
    }

    @Override
    public void addAll(@NotNull List<E> records) {
        records.forEach(this::merge);
    }

    @NotNull
    @Override
    public E persist(@NotNull E record) {
        em.persist(record);
        return record;
    }

    @NotNull
    @Override
    public E merge(@NotNull final E e) {
        em.merge(e);
        return e;
    }


    @Override
    public E remove(@NotNull E record) {
        em.remove(record);
        return record;
    }

}