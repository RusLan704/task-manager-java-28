package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;

public interface ISessionService {

    @NotNull
    SessionDTO getSession();

    void setSession(@Nullable SessionDTO session);

    void clearSession();

}
