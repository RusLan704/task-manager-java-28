package ru.bakhtiyarov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.endpoint.ISessionEndpoint;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.api.service.converter.IConverterLocator;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.exception.user.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @NotNull
    public SessionEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    public SessionEndpoint() {
        super(null);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable final Session session = serviceLocator.getSessionService().open(login, password);
        return converterLocator.getSessionConverter().toDTO(session);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void closeSession(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().close(converterLocator.getSessionConverter().toEntity(sessionDTO));
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean closeSessionAll(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getSessionService().closeAll(session);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Integer getServerPort(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getPropertyService().getServerPort();
    }

}