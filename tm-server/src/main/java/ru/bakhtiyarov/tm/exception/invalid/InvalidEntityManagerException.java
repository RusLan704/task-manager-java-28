package ru.bakhtiyarov.tm.exception.invalid;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class InvalidEntityManagerException extends AbstractException {

    @NotNull
    public InvalidEntityManagerException() {
        super("Error! Entity manager is invalid...");
    }

}